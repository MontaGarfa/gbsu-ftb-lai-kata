package com.kata.gbsuftblai.services

import org.springframework.stereotype.Component

@Component
class GbsuFtbLaiService {

    private val containsMap: Map<Char, String> = mapOf('3' to "Gbsu", '5' to "Ftb", '7' to "Lai")
    private val divisorsMap: Map<Int, String> = mapOf(3 to "Gbsu", 5 to "Ftb")


    fun convertNumber(inputNumber: Int): String {
        val result = StringBuilder()
        divisorsMap.forEach { if (inputNumber % it.key == 0) result.append(it.value) }
        inputNumber.toString().chars().mapToObj { it.toChar() }.forEach {
            if (containsMap[it] != null)
                result.append(containsMap[it])
        }
        return result.toString().ifEmpty { inputNumber.toString() }
    }


}