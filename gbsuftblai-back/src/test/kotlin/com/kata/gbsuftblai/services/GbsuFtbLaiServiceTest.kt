package com.kata.gbsuftblai.services

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GbsuFtbLaiServiceTest {
    private val gbsuFtbLaiService = GbsuFtbLaiService()
    @Test
    fun testIfNumberDivisibleOrContainsThree(){
        val result = gbsuFtbLaiService.convertNumber(3)
        assertEquals("GbsuGbsu",result)
        val result2 = gbsuFtbLaiService.convertNumber(34)
        assertEquals("Gbsu",result2)
    }
    @Test
    fun testIfNumberDivisibleOrContainsFive(){
        val result = gbsuFtbLaiService.convertNumber(5)
        assertEquals("FtbFtb",result)
        val result2 = gbsuFtbLaiService.convertNumber(52)
        assertEquals("Ftb",result2)
    }
    @Test
    fun testIfNumberContainsSeven(){
        val result = gbsuFtbLaiService.convertNumber(7)
        assertEquals("Lai",result)
    }
    @Test
    fun testIfAnotherNumber(){
        val result = gbsuFtbLaiService.convertNumber(1)
        assertEquals("1",result)
    }


}