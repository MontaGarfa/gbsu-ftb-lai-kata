import { Component, OnInit, OnDestroy } from '@angular/core';
import { element } from 'protractor';
import { Subscription } from 'rxjs';
import { GbsuFtbLaiService } from '../gbsu-ftb-lai.service';

@Component({
  selector: 'app-gbsu-ftb-lai',
  templateUrl: './gbsu-ftb-lai.component.html'
})
export class GbsuFtbLaiComponent implements OnInit, OnDestroy {
  listNumberConverted:Array<NumberConverted>;
  constructor(private gbsuFtbLaiService: GbsuFtbLaiService) { }

  ngOnInit(): void {
    this.listNumberConverted = new Array();
  }

  ngOnDestroy(): void {
    
  }

  convertNumber(inputNumber: number): void {
    this.gbsuFtbLaiService.convertNumber(inputNumber).subscribe(resultDto => {
      this.listNumberConverted.push({ numberToConvert : inputNumber, result : resultDto.result})
      console.log(this.listNumberConverted)
    })
    }

}

interface NumberConverted {
  numberToConvert: number;
  result: string;
}
