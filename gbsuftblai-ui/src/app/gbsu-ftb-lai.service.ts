import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GbsuFtbLaiService {
  url:string;
  constructor(private http:HttpClient, @Inject("SERVER_URL") url:string) {
    this.url = url;
   }
  convertNumber(inputNumber:number):Observable<any>{
    return this.http.get<any>(this.url+"/gbsu-ftb-lai/"+inputNumber);
  }

}
