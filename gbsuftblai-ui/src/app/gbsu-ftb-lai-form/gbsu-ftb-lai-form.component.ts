import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-gbsu-ftb-lai-form',
  templateUrl: './gbsu-ftb-lai-form.component.html'
})
export class GbsuFtbLaiFormComponent implements OnInit {
  inputForm: FormGroup;
  @Output()
  private submitNumberOutput: EventEmitter<number> = new EventEmitter();


  constructor(private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.inputForm = this.formBuilder.group({
      inputNumber:['',[Validators.required,Validators.min(0)]]
    })
  }

  submitNumber(): void {
    console.log(this.inputForm.get('inputNumber').value)
    this.submitNumberOutput.emit(this.inputForm.get('inputNumber').value);
    this.inputForm.reset();
  }
  get f() { return this.inputForm.controls; }

}
